#include <stdio.h>
#include <pthread.h>
#include <linux/futex.h>
#include <unistd.h>

#include <sys/syscall.h>

#include <limits.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <stdatomic.h>

/* atomic_compare_exchange_strong(ptr, oldval, newval)
   atomically performs the equivalent of:
   
   if (*ptr == *oldval)
     *ptr = newval;
   else
     *oldval = *ptr;
   
   It returns true if the test yielded true and *ptr was updated. */

#define errExit(msg) { perror(msg); exit(EXIT_FAILURE);}

#define CHILDREN 10

typedef enum {
  FTX_AVAIL,
  FTX_BUSY,
  FTX_PENDING
} Futex_State_Type;

static Futex_State_Type State;
static int Queue_Head;
static pthread_t t[CHILDREN];

static int futex(uint32_t *uaddr, int futex_op, uint32_t val,
		 const struct timespec *timeout, uint32_t *uaddr2, uint32_t val3)
{
  return syscall(SYS_futex, uaddr, futex_op, val,
		 timeout, uaddr2, val3);
}

/* Acquire the futex pointed to by 'futexp': wait for its value to
   become <val>, and then set the value to <busy>. */
static void fwait(uint32_t *futexp, Futex_State_Type val)
{
  long s;
  while (1) {
    const uint32_t requested = val;
    if (atomic_compare_exchange_strong(futexp, &requested, FTX_BUSY))
      break;      /* Yes */
    /* Futex is not in requested state, wait */
    s = futex(futexp, FUTEX_WAIT, requested, NULL, NULL, 0);
    if (s == -1 && errno != EAGAIN)
      errExit("futex-FUTEX_WAIT");
  }
}

/* Set futex in state <busy> to <val> */
static void fpost(uint32_t *futexp, Futex_State_Type val)
{
  long s;
  const uint32_t busy = FTX_BUSY;
  if (atomic_compare_exchange_strong(futexp, &busy, val)) {
    s = futex(futexp, FUTEX_WAKE, INT_MAX, NULL, NULL, 0);
    if (s  == -1)
      errExit("futex-FUTEX_WAKE");
  }
}

/* Wait for signal (updated data)*/
static void fwait_sig(uint32_t *futexp)
{
  fwait(futexp, FTX_PENDING);
}

/* Wait for data avilable*/
static void fwait_avail(uint32_t *futexp)
{
  fwait(futexp, FTX_AVAIL);
}

/* Post updated data */
static void fpost_sig(uint32_t *futexp)
{
  fpost(futexp, FTX_PENDING);
}

/* Release futex */
static void fpost_avail(uint32_t *futexp)
{
  fpost(futexp, FTX_AVAIL);
}


void* start_thread(void* arg) {
  Futex_State_Type* State = (Futex_State_Type*) arg;
  
    long tid = syscall(SYS_gettid);
  //printf("child waiting for A\n");
  //wait_on_futex_value(State, FTX_EMPTY);
  usleep(100000);
  while(1) {
    fwait_avail(State);
    // Write 0xB to the shared data and wake up parent.
    Queue_Head++;
    printf("%d, child %d writing: %d\n", *State, tid, Queue_Head);
    fpost_sig(State);
    usleep(1000000);
  }
}

int main(int argc, char** argv) {

  State = FTX_AVAIL;
  
  int i;
  for (i = 0; i < CHILDREN; i++) {
    pthread_create(&t[i], NULL, start_thread, &State);
  }
  // Parent process.
  
  int last = 0;
  while(1) {
    
    fwait_sig(&State);
    int head = Queue_Head;
    fpost_avail(&State);
    printf("%d, parent got: %6d %d\n", State, head, head-last);
    last = head;
  }
  for (i = 0; i < CHILDREN; i++) {
    pthread_join(t[i], NULL);
  }
  return 0;
}
