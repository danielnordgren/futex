#include <stdio.h>
#include <pthread.h>
#include <linux/futex.h>
#include <unistd.h>

#include <sys/syscall.h>

#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <stdatomic.h>


typedef enum {
  FTX_EMPTY,
  FTX_BUSY,
  FTP_PENDING
} Futex_State_Type;

static Futex_State_Type State = FTX_EMPTY;

static int Queue_Head;

pthread_t t1,t2;
       #define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \
                               } while (0)

static int
futex(uint32_t *uaddr, int futex_op, uint32_t val,
      const struct timespec *timeout, uint32_t *uaddr2, uint32_t val3)
{
  return syscall(SYS_futex, uaddr, futex_op, val,
		 timeout, uaddr2, val3);
}

       /* Acquire the futex pointed to by 'futexp': wait for its value to
          become 1, and then set the value to 0. */

       static void
       fwait(uint32_t *futexp)
       {
           long s;

           /* atomic_compare_exchange_strong(ptr, oldval, newval)
              atomically performs the equivalent of:

                  if (*ptr == *oldval)
                      *ptr = newval;

              It returns true if the test yielded true and *ptr was updated. */

           while (1) {

               /* Is the futex available? */
               const uint32_t one = 1;
               if (atomic_compare_exchange_strong(futexp, &one, 0))
                   break;      /* Yes */

               /* Futex is not available; wait */

               s = futex(futexp, FUTEX_WAIT, 0, NULL, NULL, 0);
               if (s == -1 && errno != EAGAIN)
                   errExit("futex-FUTEX_WAIT");
           }
       }

       /* Release the futex pointed to by 'futexp': if the futex currently
          has the value 0, set its value to 1 and the wake any futex waiters,
          so that if the peer is blocked in fwait(), it can proceed. */

       static void
       fpost(uint32_t *futexp)
       {
           long s;

           /* atomic_compare_exchange_strong() was described
              in comments above */

           const uint32_t zero = 0;
           if (atomic_compare_exchange_strong(futexp, &zero, 1)) {
               s = futex(futexp, FUTEX_WAKE, 1, NULL, NULL, 0);
               if (s  == -1)
                   errExit("futex-FUTEX_WAKE");
           }
       }


void* start_thread(void* arg) {
  Futex_State_Type* State = (Futex_State_Type*) arg;
  
    long tid = syscall(SYS_gettid);
  //printf("child waiting for A\n");
  //wait_on_futex_value(State, FTX_EMPTY);
  usleep(100000);
  while(1) {
    fwait(State);
    // Write 0xB to the shared data and wake up parent.
    Queue_Head++;
    printf("child %d writing: %d\n", tid, Queue_Head);
    fpost(State);
    usleep(10000);
  }
}

int main(int argc, char** argv) {

  State = 0;
  
  
  pthread_create(&t1, NULL, start_thread, &State);
  pthread_create(&t2, NULL, start_thread, &State);

  // Parent process.
  
  fpost(&State);
  
  int last = 0;
  while(1) {
    
    fwait(&State);
    int head = Queue_Head;
    fpost(&State);
    printf("parent got: %6d %d\n", head, head-last);
    last = head;
    usleep(10000);
  }
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  return 0;
}
