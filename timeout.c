
#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <stdio.h>
#include <pthread.h>
#include <linux/futex.h>
#include <unistd.h>
#include <time.h>

#include <sys/syscall.h>

#include <limits.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <stdatomic.h>
#include <sched.h>


/* atomic_compare_exchange_strong(ptr, oldval, newval)
   atomically performs the equivalent of:
   
   if (*ptr == *oldval)
     *ptr = newval;
   else
     *oldval = *ptr;
   
   It returns true if the test yielded true and *ptr was updated. */

#define errExit(msg) { perror(msg); exit(EXIT_FAILURE);}

#define CHILDREN 5
#define TS_LOG_SIZE 100

typedef enum {
  FTX_AVAIL,
  FTX_BUSY,
  FTX_PENDING
} Futex_State_Type;

typedef struct {
  int64_t time;
  int32_t id;
  int32_t data;
} ts_log_entry_t;

static Futex_State_Type State;
static pthread_t t[CHILDREN];

static ts_log_entry_t ts_log[ TS_LOG_SIZE ];
static int ts_pos = 0;

static int64_t nsec() {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return 1000000000 * ts.tv_sec + ts.tv_nsec;
}

static void timestamp(int32_t id, int32_t data) {
  if (ts_pos >= TS_LOG_SIZE)
    return;
  ts_log[ ts_pos ] = (ts_log_entry_t) {nsec(), id, data};
  ts_pos++;
}

static int futex(uint32_t *uaddr, int futex_op, uint32_t val,
		 const struct timespec *timeout, uint32_t *uaddr2, uint32_t val3)
{
  return syscall(SYS_futex, uaddr, futex_op, val,
		 timeout, uaddr2, val3);
}

/* Acquire the futex pointed to by 'futexp': wait for its value to
   become <val>, and then set the value to <busy>. */
static int fwait(uint32_t *futexp,
		  Futex_State_Type val,
		  const struct timespec *timeout)
{
  long s;
  while (1) {
    const uint32_t requested = val;
    if (atomic_compare_exchange_strong(futexp, &requested, FTX_BUSY))
      break;      /* Yes */
    /* Futex is not in requested state, wait */
    s = futex(futexp, FUTEX_WAIT, requested, timeout, NULL, 0);
    if (s == -1 && errno == ETIMEDOUT)
      return 1;
    if (s == -1 && errno != EAGAIN)
      errExit("futex-FUTEX_WAIT");
  }
  return 0;
}

/* Set futex in state <busy> to <val> */
static void fpost(uint32_t *futexp, Futex_State_Type val)
{
  long s;
  const uint32_t busy = FTX_BUSY;
  if (atomic_compare_exchange_strong(futexp, &busy, val)) {
    s = futex(futexp, FUTEX_WAKE, INT_MAX, NULL, NULL, 0);
    if (s  == -1)
      errExit("futex-FUTEX_WAKE");
  }
}

/* Wait for data avilable*/
static void fwait_avail(uint32_t *futexp)
{
  fwait(futexp, FTX_AVAIL, NULL);
}

/* Wait for signal or timeout and avail*/
static int fwait_sig(uint32_t *futexp, const struct timespec *timeout)
{
  if (fwait(futexp, FTX_PENDING, timeout)) {
    // Timeout, take the futex to busy
    const uint32_t requested = FTX_AVAIL;
    if (atomic_compare_exchange_strong(futexp, &requested, FTX_BUSY))
      return 1;
    // Unable to aquire the futex, someone is writing, wait for pending
    fwait(futexp, FTX_PENDING, NULL);
  }
  return 0;
}

/* Post updated data */
static void fpost_sig(uint32_t *futexp)
{
  fpost(futexp, FTX_PENDING);
}

/* Release futex */
static void fpost_avail(uint32_t *futexp)
{
  fpost(futexp, FTX_AVAIL);
}


static void sched_setup() {
  cpu_set_t set;
	   
  const struct sched_param param = {10};
  sched_setscheduler(0, SCHED_FIFO, &param);
  CPU_ZERO(&set);
  CPU_SET(3, &set);
  sched_setaffinity(0, sizeof(set), &set);
}

void* start_thread(void* arg) {
  
  long tid = (long) arg;
  
  int64_t start,dur;
  
  sched_setup();
  
  //printf("child waiting for A\n");
  //wait_on_futex_value(State, FTX_EMPTY);
  usleep(1000000);
  while(1) {
    start = nsec();
    fwait_avail(&State);
    dur = nsec() - start;
    timestamp(tid, dur);
    fpost_sig(&State);
    start = nsec();
    fwait_avail(&State);
    dur = nsec() - start;
    timestamp(tid, dur);
    fpost_sig(&State);
    if (ts_pos >= TS_LOG_SIZE)
      break;
    usleep(400000);
  }
}

int main(int argc, char** argv) {

  struct timespec timeout = {0, 100000000};
  State = FTX_AVAIL;
  int i;
  
  sched_setup();
  int64_t last_t = nsec();
  for (i = 0; i < CHILDREN; i++) {
    pthread_create(&t[i], NULL, start_thread, (void*)(long)i);
  }
  // Parent process.

  i = 0;
  while(1) {
    int timedout = fwait_sig(&State, &timeout);
    timestamp(-1, timedout);
    fpost_avail(&State);
    if (ts_pos >= TS_LOG_SIZE)
      break;
  }
  for (i = 0;i < ts_pos;i++) {
    ts_log_entry_t* ts_p = &ts_log[i];
    int64_t t = ts_p->time;
    printf("%6d %18ld %10ld %4d %4d\n", i, t, t - last_t, ts_p->id, ts_p->data);
    last_t = t;
  }
  return 0;
}
